package pl.ti.Technologie.Internetowe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ti.Technologie.Internetowe.model.Shopping;
import pl.ti.Technologie.Internetowe.repository.ShoppingRepository;

import java.util.List;

@Service
public class ServiceList {

  final
  private ShoppingRepository shoppingRepository;

  @Autowired
  public ServiceList(ShoppingRepository shoppingRepository) {
    this.shoppingRepository = shoppingRepository;
  }

  public List<Shopping> getList(){
    return shoppingRepository.findAll();
  }

  public int addItem(Shopping shopping){
    return shoppingRepository.save(shopping);
  }

  public void deleteItem(int id) {
    shoppingRepository.deleteById(id);
  }
}
