package pl.ti.Technologie.Internetowe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ti.Technologie.Internetowe.model.Shopping;

@Repository
public interface ShoppingRepository extends JpaRepository<Shopping,Integer> {
}
