package pl.ti.Technologie.Internetowe.model;

import lombok.*;

import javax.persistence.*;

@Data
@Table
@Entity
@NoArgsConstructor
@Getter
@Setter
public class Shopping {

  @Id
          @GeneratedValue
  int id;
  String name;
  int number;

  public Shopping(String name, int number) {
    this.name = name;
    this.number = number;
  }

}
