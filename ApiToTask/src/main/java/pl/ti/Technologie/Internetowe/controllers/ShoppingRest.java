package pl.ti.Technologie.Internetowe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ti.Technologie.Internetowe.model.Shopping;
import pl.ti.Technologie.Internetowe.service.ServiceList;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class ShoppingRest {

  private final ServiceList service;

  @Autowired
  public ShoppingRest(ServiceList service) {
    this.service = service;
  }

  @GetMapping
  public List<Shopping> getList (){
  return service.getList();
  }

  @PostMapping
  public int saveItem(@RequestBody Shopping item){
  service.addItem(item);

  return value;
  }

  @DeleteMapping("/{id}")
  public void deleteItem(@PathVariable int id){
  service.deleteItem(id);
  }
}
