import React, { Component } from 'react';

class Table extends Component {

  constructor(props){
    super(props);
    this.state={
      items:[],
      isLoaded:false,
    }

    this.addItem=this.addItem.bind(this);
    this.saveToMySql=this.saveToMySql.bind(this);
    this.deleteItem=this.deleteItem.bind(this);
    this.componentDidMount=this.componentDidMount.bind(this);
  }

  componentDidMount(){
    fetch('http://localhost:8080/api')
      .then(res => res.json())
      .then(json => {
        this.setState({
          isLoaded: true,
          items: json,
        })
      });
  }

  saveToMySql(value,name,number){
    fetch('http://localhost:8080/api', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "id":value,
          "name": name,
          "number": number
      })
    })
  }

  addItem(e){
    if(this._name !== "" && this._number !== ""){
      
    this.setState((prevState) => {
      var newItem = {
        id:prevState.items.length,
        name:this._name.value,
        number:this._number.value
      }
      this.saveToMySql(prevState.items.length,this._name.value,this._number.value);

      return  {
        items:prevState.items.concat(newItem)
      }
    });
  }
}

  deleteItem(item){
    fetch('http://localhost:8080/api/'+item.id, {
      method: 'DELETE',
      headers: {},
      body: JSON.stringify({})})

    this.setState((prevState) => {
      var index = prevState.items.indexOf(item);
      delete prevState.items[index]

      return{
      items:prevState.items
      }
    });
  }

  render() {

    var{isLoaded,items} = this.state;

    if(!isLoaded){
      return <div>Loading...</div>
    }

    else{
      return (
        <div className="table">
          <div class="input-group">
              <input type="text" ref={(name) => this._name=name}
                     placeholder="name"></input>
              <input type="number" ref={(number) => this._number = number}
                       id="number" ></input>
              <button class="btn btn-success" onClick={()=>{this.addItem(this._name,this._number)}} type="submit">ADD</button>
          </div>
          <div id="tab">
            <table class="table table-striped">
              <thead class="thead-dark" id="the">
                <tr>
                  <th>Name</th>
                  <th>Number</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {items.map(item => (
                    <tr key={item.id}>
                      <td>{item.name}</td>
                      <td>{item.number}</td>
                      <td><button class="btn btn-danger" id ="deleteButton"onClick={()=>{this.deleteItem(item)}}>Delete</button></td>
                    </tr>               
                ))}
              </tbody>
            </table>
            </div>
        </div>
      );
    }
  }
}

export default Table;
